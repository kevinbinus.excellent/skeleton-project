# skeleton-project

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### PACKAGE JSON

package.json adalah file untuk menampung semua depedencies,modul,library yang terinstall dalam folder project

### SRC

src adalah folder yang berfungsi untuk menampung semua file / folder yang akan digunakan untuk menulis code

### ASSETS

didalam folder src terdapat folder yang bernama assets, berfungsi untuk menampung file gambar, dan bisa untuk menampung file scss

### COMPONENTS

folder components yang terdapat didalam folder src berfungsi untuk menampung component yang bisa digunakan secara global

### VIEWS

folder views terdapat didalam folder src berfungsi untuk menampung file tampilan
